import Reflux from 'reflux';
import Actions from 'flux/actions';
import WpApi from 'services/wp-api';
import _ from 'lodash';

class WordpressStore extends Reflux.Store
{
    constructor()
    {
        super();
        this.state = {loading:true, error:null, data:[]};
        this.listenTo(Actions.getPosts, this.onGetPosts);
        this.listenTo(Actions.getPostsSuccess, this.onGetPostsSuccess);
        this.listenTo(Actions.getPostsFailed, this.onGetPostsFailed);
    }

    onGetPosts(data)
    {
        console.log(data);
        this.setState({loading:true});
        WpApi.getPosts();
    }

    onGetPostsSuccess(obj) {
        console.log('onGetPostsSuccess', obj);
        let data = this.state.data;
        data.push(obj);
        this.setState({loading:false, data:data});
    }

    onGetPostsFailed(data) {
        console.log('onGetPostsFailed', data);
        this.setState({loading:false, error:true});
    }
}

export default WordpressStore;
