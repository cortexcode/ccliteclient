import Reflux from 'reflux';

const Actions = Reflux.createActions([
    "getPosts",
    "getPostsSuccess",
    "getPostsFailed"
]);

export default Actions;
