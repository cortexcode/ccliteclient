import Actions from 'flux/actions';
import config from 'config';
import wpApi from 'wpapi';
import _ from 'lodash';

const WpApi = {

    getPosts() {
        console.log("getting psts w/ ajax");
        var wp = new wpApi({ endpoint: config.wpUrl });

        wp.categories().slug(config.wpCategory)
            .then(function(categories) {
                const category = categories[0];
                return wp.posts().categories(category.id).perPage(50);
            })
        .then(function(postsinCategory) {
            _.each(postsinCategory, (p, i) => {
                wp.posts().id(p.id)
                    .then(function(result) {
                        if (result.content) {
                            Actions.getPostsSuccess({ id : p.id, content : result.content.rendered });
                        }
                        else {
                            Actions.getPostsFailed();
                        }
                    });
            });

        });



    }

};

export default WpApi;
