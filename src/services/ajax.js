var $ = require('jquery');

module.exports =  {

		/* Do an AJAX request */
		doRequest : function(rMethod, rData, rUrl, callback, headerKey, headerValue) {

			var rDataType = 'json';

			var request = $.ajax({
			  url: rUrl,
			  method: rMethod,
			  data: rData,
			  dataType: rDataType,
				beforeSend: function (request)
				{
					if ( headerKey && headerValue ) {
						request.setRequestHeader(headerKey, headerValue);
						}
				},
			});

			this.addCallbacks(request, callback);

		},

		addCallbacks: function(request, callback){

			request.done(function( msg ) {
				callback(null, msg);
			});

			request.fail(function( jqXHR, textStatus ) {
				if ( jqXHR.responseJSON ) {
					if ( jqXHR.responseJSON.error ) {
						callback(jqXHR.responseJSON.error.message, null);
						}
					else if ( jqXHR.responseJSON ){
						callback(jqXHR.responseJSON, null);
					}
				}
				else {
					callback(textStatus, null);
				}
			});

		},

		doFileRequest: function(rData, rUrl, callback, headerKey, headerValue) {

			var request =  $.ajax({
				    url: rUrl,
				    data: rData,
				    cache: false,
				    contentType: false,
				    processData: false,
				    type: 'POST',

					beforeSend: function (request)
					{
						if ( headerKey && headerValue ) {
							request.setRequestHeader(headerKey, headerValue);
							}
					},

				});

				this.addCallbacks(request, callback);

		},

};
