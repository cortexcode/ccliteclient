import React from 'react';
import { Component } from 'reflux';
import Actions from 'flux/actions';
import WordpressStore from 'flux/wordpress-store';
import { Grid, Row } from 'react-bootstrap';
import _ from 'lodash';
import { Col } from 'react-bootstrap';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup'

class RootPage extends Component {

    constructor(props) {
        super(props);
        this.store = WordpressStore;
        this.onClickColumn = this.onClickColumn.bind(this);
    }

    componentDidMount() {
        Actions.getPosts();
        this.setState({visibleColumns : [0,1,2]});
    }

    onClickColumn(c) {
        console.log('click!');
        /*const newArr = _.pull(this.state.visibleColumns, c);
        this.setState({visibleColumns : newArr});*/
    }

    render() {
        const loading = this.state.loading;
        const error = this.state.error;
        if (error) {
            return (
                <div style={{padding: '1em'}}>
                    <center><span>error</span></center>
                </div>
            );
        }
        if (loading === true) {
            return (
                <div style={{padding: '1em'}}>
                    <center><div className="spinner">|</div></center>
                </div>
            );
        }

        let columns = [];

        for (let i=0; i<3; i++) {
            let columnContent = null;
            if (_.indexOf(this.state.visibleColumns, i) !== -1) {
                columnContent = <Col className="column"
                    onClick={()=>{this.onClickColumn(i);}}
                    key={this.state.data[i].id} xs={6} md={4}
                    dangerouslySetInnerHTML={{ __html: this.state.data[i].content }} />;
            }

            const newColl =
            <CSSTransitionGroup
                key={'transition-column-'+i}
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={true}
                transitionLeaveTimeout={300}>
                {columnContent}
            </CSSTransitionGroup>;
            columns.push(newColl);

        }

        return (
            <div style={{padding: '1em'}}>
                <Grid>
                    <Row key="rootRow" className="show-grid">
                        {columns}
                    </Row>
                </Grid>
            </div>

        );


    }

}

export default RootPage;
