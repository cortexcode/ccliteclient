import React, { Component } from 'react';
import './App.css';
import './styles/bootstrap.css';
import './styles/bootstrap.sandstone.css';
import RootPage from 'pages/root';

class App extends Component {
  render() {
    return (
      <div className="App">
          <RootPage />
      </div>
    );
  }
}

export default App;
