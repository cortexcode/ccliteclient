import React, { Component } from 'react';
import { Col } from 'react-bootstrap';

class Column extends Component {

    render() {
            return <Col key={this.props.id} xs={6} md={4} dangerouslySetInnerHTML={{ __html: this.props.content }}></Col>;
    }

}

export default Column;
